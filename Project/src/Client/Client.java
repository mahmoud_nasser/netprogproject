package Client;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JOptionPane;

import Server.CCReceiver;
import Server.ClientConnection;
import Server.Server;

public class Client extends Thread{
	String server = "locahost";
	int port = 9000;
	Socket socket = null;
	private CReceiver rec;
	private OutputStream output;
	public String requested_local_user = "";

	private boolean isUserNameChecked = false;
	private boolean isUserNameReady = false;


	public Client(String server, String port){
		this.server = server;
		try{
			this.port = Integer.parseInt(port);

		}catch(NumberFormatException e){
			System.out.println("Portnumber argument was invalid");
			System.exit(-1);
		}
	}
	public synchronized void sendMessage(String msg) {
		try {
			OutputStreamWriter ow = new OutputStreamWriter(output);
			BufferedWriter bw = new BufferedWriter(ow);
			System.out.println("Sending: "+msg);
			bw.write(msg+"\r\n");
			bw.flush();
			/*
			output.write((msg).getBytes());
			output.flush();*/
		} catch (IOException e) {
			//Closed connection? Disconnected!
			//terminate();
			e.printStackTrace();
			System.exit(-1);
		}
	}
	public synchronized void announceUsernameToServer(String username){
		isUserNameChecked = false;
		isUserNameReady = false;
		sendMessage(ClientConnection.C_SET_USERNAME+"##"+username);
	}
	/*protected synchronized boolean usernameIsChecked(){
		return isUserNameChecked;
	}
	protected synchronized boolean usernameIsReady(){
		return isUserNameReady;
	}*/
	public synchronized boolean isUserNameUnique() throws InterruptedException{
		while(!(isUserNameReady || isUserNameChecked)){
			wait();
		}
		if(isUserNameChecked && isUserNameReady){
			isUserNameReady = false;
			isUserNameChecked = false;
			return true;
		}
		else{
			isUserNameReady = false;
			isUserNameChecked = false;
			return false;
		}
	}

	@Override
	public void run(){
		try {
			socket = new Socket(server, port);
			output = socket.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			rec = new CReceiver(br, this);
			rec.start();
			System.out.println("Sucessfully connected to "+ server+":"+port);
		}catch (UnknownHostException e){
			System.out.println(e);
			System.out.println("Server could not be found, check your parameters or try again later");
			System.exit(-1);
		}
		catch (IOException e) {
			System.out.println(e);
			System.out.println("Client: Could not connect to server");
			System.exit(-1);
		}
		catch(IllegalArgumentException e){
			System.out.println(e);
			System.out.println("Portnumber was not valid. A valid portnumber ranges from 0 to 65535");
			System.exit(-1);
		}
		if(!socket.isClosed()){
			requested_local_user = JOptionPane.showInputDialog(null, "Välj ditt smeknamn", "", -1);
			if(requested_local_user == null){
				System.exit(0);
			}
			try {
				sendMessage(ClientConnection.C_SET_USERNAME+"##"+requested_local_user);
				GameMonitor.gameMonitor.waitForUserNameValidation();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		while(!socket.isClosed()){
			//pre-game, await username declaration...
			if(!socket.isClosed()){
				try {
					sendMessage(ClientConnection.C_GET_DICTIONARY+"");
					GameMonitor.gameMonitor.getDictionary();
					GameMonitor.gameMonitor.awaitGameStartRequest();
					sendMessage(ClientConnection.C_START_GAME+"");
					GameMonitor.gameMonitor.waitForGameStart();
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.out.println("Unexpected error ocured...");
					System.exit(-1);
				}
			}
			//whilesatsen här är under spelets gång
			while(!socket.isClosed() && GameMonitor.gameMonitor.gameIsRunning()){
				sendMessage(ClientConnection.C_SET_ADVANCEMENT+"##"+GameMonitor.gameMonitor.waitForPercentUpdate());
				if(GameMonitor.gameMonitor.getCurrentPercent()>=100){					
					sendMessage(ClientConnection.C_REPORT_RACETIME+"##"+GameMonitor.gameMonitor.waitForReadyRaceTime());
					//det skickas men del
				}

			} //har är staten att du är färdig, men spelet fortfarande igång (hos servern spel färdigt när alla är färdiga
			while(!socket.isClosed() && GameMonitor.gameMonitor.gameIsRunning() && GameMonitor.gameMonitor.IhaveFinishedTheRace()){
				sendMessage(ClientConnection.C_REPORT_RACETIME+"##"+GameMonitor.gameMonitor.waitForReadyRaceTime());//Wait until server announces "end of game"
			}
			if(!socket.isClosed() && !GameMonitor.gameMonitor.gameIsRunning()){
				GameMonitor.gameMonitor.waitForServerToTellUsWhoTheWinnerIs();
				//Wait until server announces a winner, Notice, gui will announce the winner
			}
		}
	}
	public synchronized void execute(String command){
		String[] strings = command.split("##");
		int com = -1;
		try{
			com = Integer.parseInt(strings[0].trim());
		}catch(NumberFormatException e){
			System.out.println("Command was not a valid integer");
		}
		catch(Exception e){
			System.out.println("Other error...");
		}
		switch(com){
		case -1 :
			System.out.println("CExecute: Command Parse Failure");
			break;
		case ClientConnection.G_TERMINATE :
			System.out.println("Server ordered termination... Exiting in 5 seconds");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.exit(0);
			break;
		case ClientConnection.S_USER_DISCONNECTED :
			if(strings.length < 2){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			if(!GameMonitor.gameMonitor.gameIsRunning()){
				GameMonitor.gameMonitor.removeRacer(strings[1]);
			}
			else{
				GameMonitor.gameMonitor.disconnectRacer(strings[1]);
			}
			break;
		case ClientConnection.S_NEW_USER :
			//TODO
			if(strings.length < 3){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			try{
				int color = Integer.parseInt(strings[2]);
				if(color < 0 || color > 7){
					throw new NumberFormatException();
				}
				if(strings[1].equals(requested_local_user)){
					isUserNameReady = true;
					isUserNameChecked = true;
					GameMonitor.gameMonitor.setLocalUser(requested_local_user);
					//notifyAll();
				}

				GameMonitor.gameMonitor.addRacer(strings[1], GameElements.getColorFromInt(color));
			}
			catch(NumberFormatException e){
				System.out.println("Servern skickade meddelande med ogiltiga argument!");
				System.exit(-1);
			}
			break;
		case ClientConnection.S_REJECTED :
			//TODO
			break;
		case ClientConnection.S_ERROR :
			//TODO
			break;
		case ClientConnection.S_GAME_STARTED :
			System.out.println("Execution Game Start");
			if(strings.length < 2){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			try{
				boolean gameStarted = strings[1].equals("1");//(boolean) Boolean.parseBoolean(strings[1]);
				if(gameStarted){
					System.out.println("Game is started");
					GameMonitor.gameMonitor.startRace();
				}else{
					GameMonitor.gameMonitor.firstPlayerInGoal();
				}
			}catch(Exception e){
				System.out.println("Servern skickade meddelande med ogiltiga argument!");
				System.exit(-1);
			}
			break;
		case ClientConnection.S_SET_ADVANCEMENT :
			if(strings.length < 3){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			try{
				int percent = Integer.parseInt(strings[2]);
				if(percent < 0 || percent > 100){
					throw new NumberFormatException();
				}
				GameMonitor.gameMonitor.moveTurtle(strings[1], percent);
			}
			catch(NumberFormatException e){
				System.out.println("Servern skickade meddelande med ogiltiga argument!");
				System.exit(-1);
			}
			break;
		case ClientConnection.S_USERNAME_NONVALID :
			isUserNameChecked = true;
			isUserNameReady = false;
			notifyAll();
			requested_local_user = JOptionPane.showInputDialog(null,"Smeknamnet du angav är upptaget, försök med ett annat!", "Smeknamn...", -1);
			if(requested_local_user == null){
				System.exit(0);
			}
			sendMessage(ClientConnection.C_SET_USERNAME+"##"+requested_local_user);
			break;
		case ClientConnection.S_DICTIONARY :
			if(strings.length < 2){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			try{
				GameMonitor.gameMonitor.setDictionary(GameElements.getDictionary(Integer.parseInt(strings[1])));
			}catch(NumberFormatException e){
				System.out.println("Servern skickade meddelande med ogiltiga argument!");
				System.exit(-1);
			}
			break;
		case ClientConnection.S_WINNER :
			if(strings.length < 3){
				System.out.println("Servern skickade ogiltigt meddelande!");
				System.exit(-1);
			}
			try{
				long timeMillis = Long.parseLong(strings[2]);
				if(timeMillis < 0){
					throw new NumberFormatException();
				}
				GameMonitor.gameMonitor.publishFinishTime(strings[1], timeMillis);
			}
			catch(NumberFormatException e){
				System.out.println("Servern skickade meddelande med ogiltiga argument!");
				System.exit(-1);
			}
			break;

		}
	}

	public static void main(String[] args) {
		Client cl;
		if(args.length>0){
			//User runs program from terminal...
			if(args.length != 2){
				System.out.println("Please insert two and only two arguments\n1) Server adress/IP\n2) Port\n");
				System.exit(-1);
			}
			cl = new Client(args[0], args[1]);
		}
		else{
			//GUI asks to connect to specific server, alternativly start to a predefined server
			cl = new Client("localhost", "5000");
		}
		 
		GameMonitor monitor = new GameMonitor();
		GUI comp = new GUI(monitor);
		WordCorrector wc = new WordCorrector();
		cl.start();
		wc.start();
	}

}
