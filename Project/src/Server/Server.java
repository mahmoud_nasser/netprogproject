package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Stack;

import Client.CReceiver;
import Client.GameElements;

public class Server extends Thread {

	public static Server server;
	private int serverPort;
	private ServerSocket serverSocket = null;
	private boolean isStopped = false;
	private Thread runningThread = null;
	private ArrayList<ClientConnection> runningThreads;
	private Stack<Integer> availableColors;
	//	Event/state flags
	private boolean newConnection = false;
	private boolean gameIsRunning = false;
	private boolean winnerIsDetermined = false;
	private boolean resetIsRequested = false;
	//Game
	private String winner = "";
	private long winTimeMillis = -1;
	private int gameTextID = -1;

	public Server(int port){
		this.serverPort = port;
		Server.server = this;
		runningThreads = new ArrayList<ClientConnection>();
		availableColors = new Stack<Integer>();
		for(int n = 0; n < GameElements.COUNT_COLORS; ++n)
			availableColors.push(n);
		gameTextID = (int)(Math.random()*GameElements.COUNT_TEXTS);


	}
	/**
	 * Notifies all ClientConnections whom is suposedly waiting in the waitForGameFinish() method
	 * while a game is running
	 */
	protected synchronized void notifyServerOnNewRaceTime() {
		notifyAll();
	}
	/**
	 * Checks wether a propsed username is available 
	 * @param username The proposed username to check
	 * @return Returns true if this is the case
	 */
	public synchronized boolean isUserNameAvailableAndValid(String username){
		for(ClientConnection e : runningThreads){
			if(e.getUserName().toLowerCase().equals(username.toLowerCase()) || username.trim().equals("")||username.equals("null")){
				return false;
			}
		}
		return true;
	}
	/**
	 * This flag will be set if the server has been asked to terminate itself
	 * @return Returns true if the flag is set
	 */
	private synchronized boolean isStopped() {
		return this.isStopped;
	}
	/**
	 * Blocks until there is a termination order for the server
	 */
	private synchronized void waitForServerTermination() {
		try {
			while(!isStopped){
				wait();
			}
		}
		catch (InterruptedException e) {
		}
	}
	/**
	 * Orders termination of server
	 */
	private synchronized void stopExecution() {
		this.isStopped = true;
		notifyAll();
	}
	/**
	 * Initiates the ServerSocket
	 */
	private synchronized void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port "+serverSocket, e);
		}
	}

	/**
	 * Subroutine for the ClientConnection.terminate() method.
	 * This helps the sister method to de-register and broadcast a client disconnection
	 * @param me Instance of the ClientConnection instance that corresponds to the disconnected client
	 */
	protected synchronized void terminateMe(ClientConnection me){
		if(me.getUserColor() != -1){
			if(me.getRaceTime() != -1 && gameIsRunning){
				if(winTimeMillis > me.getRaceTime()){
					winTimeMillis = me.getRaceTime();
					winner = me.getUserName();
				}
			}
			availableColors.push(me.getUserColor());
			runningThreads.remove(me);
			sendMessageToAll(ClientConnection.S_USER_DISCONNECTED+"##"+me.getUserName());
		}

		System.out.println(Server.server.runningThreads.size()+" client(s) are now online");
		if(Server.server.runningThreads.size()==0){
			System.out.println("No active connections to the server :(\n(Reseting)");
			resetToInitialState();
			//reset Server state
		}

	}
	/**
	 * Notifies on a new valid connection to be broadcasted
	 */
	protected synchronized void announceNewConnection(){
		newConnection = true;
		notifyAll();
	}
	/**
	 * Sends commands to a newly connected client to give a heads-up on already connected co-players
	 * @param requester Instance of the ClientConnection corresponding the requesting client
	 */
	public synchronized void discoverAlreadyConnectedUsers(ClientConnection requester){
		String user = requester.getUserName();
		for(ClientConnection cc : runningThreads){
			if(!cc.getUserName().equals(user) && !cc.getUserName().equals("")){
				requester.sendMessage(ClientConnection.S_NEW_USER+"##"+cc.getUserName()+"##"+cc.getUserColor());
			}
		}
	}
	/**
	 * Blocks until a player new player connects and needs to be announced (broadcast).
	 * Returns when the game starts or if the server is in a post-game state
	 */
	public synchronized void updatePlayersUntilGameStart() {	
		while(!newConnection && !gameIsRunning && winTimeMillis == -1){
			try {
				if(gameIsRunning){
					return;
				}
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		for(ClientConnection cc : runningThreads){
			if(!cc.getAnnouncement() && !cc.getUserName().equals("") ){
				sendMessageToAll(ClientConnection.S_NEW_USER+"##"+cc.getUserName()+"##"+cc.getUserColor());
				cc.setAnnouncement(true);
			}
		}
		newConnection = false;

	}
	/**
	 * Broadcasts a message to all registered/connected clients
	 * @param message The message to be broadcasted
	 */
	public synchronized void sendMessageToAll(String message) {
		for(ClientConnection client : runningThreads){
			client.sendMessage(message);
		}
	}
	/**
	 * Answers whether the server has a running game in progress or not
	 * @return Returns true if a game is active and running.
	 */
	protected synchronized boolean isGameStarted(){
		return gameIsRunning;
	}
	/**
	 * Broadcasts and sets server in game-mode. Also wakes potentially blocked threads 
	 */
	protected synchronized void startGame(){
		if(!gameIsRunning){
			gameIsRunning = true;
			sendMessageToAll(ClientConnection.S_GAME_STARTED+"##"+1);
			notifyAll();
		}
	}
	/**
	 * Sets/updates inner attributes for the winner to be
	 * @param username Name of the (potential) winner
	 * @param timeInMillis Time the race was completed by the (potential) winner in milliseconds
	 */
	protected synchronized void setWinner(String username, long timeInMillis){	
		//vänta på att alla klienter har skickat en finish time
		this.winTimeMillis = timeInMillis;
		this.winner = username;
	}
	/**
	 * Gives the current winning time in milliseconds 
	 * @return Returns the time the race was completed by the (potential) winner in milliseconds, returns -1 if no (potential) winner has been set.
	 */
	protected synchronized long getWinningTime(){
		return winTimeMillis;
	}
	/**
	 * Answers the ID of the piece of text that the race to be run consists of
	 * @return Returns a text ID
	 */
	public synchronized int getTextID(){
		return gameTextID;
	}
	/**
	 * Blocks until all playing clients has reported a time (ingame-disconnected clients should have "reported" an infinity value)
	 */
	public synchronized void waitForGameFinish() {
		while(!hasAllReportedTime()){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(gameIsRunning){
			gameIsRunning = false;
			sendMessageToAll(ClientConnection.S_GAME_STARTED+"##"+0);
			notifyAll();
		}
	}
	/**
	 * Checks if at least one of the clients has reported a race time
	 * @return Returns true if that is the case
	 */
	private boolean hasAllReportedTime() {
		for(ClientConnection cc : runningThreads){
			if(cc.getRaceTime() == -1){
				return false;
			}
		}
		return true;
	}
	/**
	 * Compare all client's result time and sets and announces the winner to all clients only if it hasn't already been done
	 */
	protected synchronized void determineTheWinner(){
		if(winnerIsDetermined){
			return;
		}else{
			for(ClientConnection cc : runningThreads){
				if(winTimeMillis == -1){
					winTimeMillis = cc.getRaceTime();
					winner = cc.getUserName();
				}
				if(winTimeMillis > cc.getRaceTime()){
					winTimeMillis = cc.getRaceTime();
					winner = cc.getUserName();
				}
			}
			sendMessageToAll(ClientConnection.S_WINNER+"##"+winner+"##"+winTimeMillis);
			winnerIsDetermined = true;
			notifyAll();
		}
	}

	@Override
	public void run(){
		//Thread init phase
		openServerSocket();
		System.out.println("Awaiting connections...");
		//Thread operation...
		/* *****Correct way to stop the while-loop is by calling the following two lines******\
		stopExecution();
		continue;
		\************************************************************************************/
		while(!isStopped()){
			Socket clientSocket = null;
			try {
				//.accept() method blocks until someone connects
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				if(isStopped()) {
					stopExecution();
					continue;
				}
				throw new RuntimeException("Error accepting client connection", e);
			}
			addNewCon(clientSocket);
			System.out.println(Server.server.runningThreads.size()+" client(s) is now online");
		}
		//Server Thread termination phase

		//return;

	}


	private void addNewCon(Socket clientSocket){
		ClientConnection cc = new ClientConnection(clientSocket);
		cc.start();
		if(availableColors.empty() || gameIsRunning){
			cc.sendMessage(ClientConnection.S_REJECTED+"");
			System.out.println("Server was full or had a running game!");
			cc.terminate();
			return;
		}
		cc.setUserColor(availableColors.pop());
		runningThreads.add(cc);
		System.out.println("New Connection Acquired");
	}
	protected synchronized void resetToInitialState() {
		newConnection = false;
		gameIsRunning = false;
		winnerIsDetermined = false;
		resetIsRequested = false;
		String winner = "";
		winTimeMillis = -1;
		gameTextID = (int)(Math.random()*GameElements.COUNT_TEXTS);
	}
	protected synchronized void requestMyTermination(ClientConnection cc) {
			cc.sendMessage(ClientConnection.G_TERMINATE+"");
			cc.terminate();
	}

	public static void main(String[] args){

		int portNr = -1;
		try{
					//	portNr = Integer.parseInt(args[0]);
			if(portNr <= 0){
			//				throw new NumberFormatException();
			}
		}catch(NumberFormatException e){
			System.out.println("Argument was not a valid unsigned integer\nTry again...");
			System.exit(-1);
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("No arguments, please try again, and input a portnumber to run on.");
			System.exit(-1);
		} 
		portNr = 5000; //Delete this row when enabling "args"

		Server.server = new Server(portNr);
		Server.server.start();
		Server.server.waitForServerTermination();
		System.out.println("Server-thread has terminated.\nClosing...");
		System.exit(0);
	}






}
