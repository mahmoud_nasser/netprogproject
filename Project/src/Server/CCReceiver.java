package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CCReceiver extends Thread{
	BufferedReader br;
	ClientConnection myCC;
	public CCReceiver(BufferedReader br, ClientConnection cl){
		this.br = br;
		this.myCC = cl;
	}

	@Override
	public void run() {
		try {
			String msg = br.readLine();
			while(msg != null){
				System.out.println("Recieved: "+msg);
				myCC.execute(msg);
				msg = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("Lost connection to client, terminating...");
			try {
				myCC.terminate();
				br.close();
			} catch (IOException e1) {
				System.out.println("Something really bad happened!");
				e1.printStackTrace();
			}
		}
	}
	public synchronized void close(){
		try {
			br.close();
		} catch (IOException e1) {
			System.out.println("Something really bad happened!");
			e1.printStackTrace();
		}
	}
}
