package Client;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;

public class GUI extends JFrame implements Observer, KeyListener {
	public GameMonitor monitor;
	public static final int GAME_HEIGHT = 100;
	public int last_num_racers = 0;
	ArrayList<JLabel> labels = new ArrayList<JLabel>();
	JLabel text = null;
	String dictionary = "";
	final static int width = 880;
	final static int height = 500;
	public GUI(final GameMonitor monitor) {	
		 try {
	            // Set System L&F
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch(Exception e){
	    	
	    }
		this.monitor = monitor;
		this.monitor.addObserver(this);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setFocusable(true);
		super.setPreferredSize(new Dimension(width, height));
		super.addKeyListener(this);
		super.setLayout(new BorderLayout());
		JPanel labelPanel = new JPanel();
		final JButton startButnto = new JButton("Starta Spelet!");
		JLabel label1 = new JLabel("", null, JLabel.CENTER);
		JLabel label2 = new JLabel("", null, JLabel.CENTER);
		JLabel label3 = new JLabel("", null, JLabel.CENTER);
		JLabel label4 = new JLabel("", null, JLabel.CENTER);
		labelPanel.setFocusable(false);
		label1.setFocusable(false);
		label2.setFocusable(false);
		label3.setFocusable(false);
		label4.setFocusable(false);
		startButnto.setFocusable(false);
		//	    try {
		//	    	dictionary = (monitor.getDictionary());
		text = new JLabel("Väntar på ett användarnamn...");
		Thread thr = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					setTitle("TypeRacer 2000");
					while(dictionary.equals("")){
						text.setText("Väntar på ett användarnamn");
						Thread.sleep(300);
						if(!dictionary.equals("")){
							break;
						}
						text.setText("Väntar på ett användarnamn.");
						Thread.sleep(300);
						if(!dictionary.equals("")){
							break;
						}
						text.setText("Väntar på ett användarnamn..");
						Thread.sleep(300);
						if(!dictionary.equals("")){
							break;
						}
						text.setText("Väntar på ett användarnamn...");
						Thread.sleep(300);
					}
					setTitle("TypeRacer 2000 - "+monitor.getLocalUserName());
					monitor.waitForGameStart();

					monitor.waitForGameEnd();
					while(GameMonitor.gameMonitor.currPercent<100){
						Thread.sleep(400);
					}
					
					text.setText(" ");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thr.start();
		
		//		} catch (InterruptedException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}
		labels.add(label1);
		labels.add(label2);
		labels.add(label3);
		labels.add(label4);
		labelPanel.add(label1);
		labelPanel.add(label2);
		labelPanel.add(label3);
		labelPanel.add(label4);

		Border paddingBorder = BorderFactory.createEmptyBorder(10,10,10,10);

		text.setBorder(BorderFactory.createCompoundBorder(null,paddingBorder));
		text.setFont(new Font("SansSerif", Font.BOLD, 20));

		labelPanel.setLayout(new GridLayout(0,4));
		super.getContentPane().add(labelPanel, BorderLayout.NORTH);


		super.getContentPane().add(startButnto, BorderLayout.EAST);

		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(0,1));

		bottom.add(text);
		bottom.add(startButnto);

		super.getContentPane().add(bottom, BorderLayout.SOUTH);

		
		startButnto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				monitor.requestGameStart();
				startButnto.setEnabled(false);

			}
		});
		this.setResizable(false);
		super.pack();
		super.setVisible(true);
		super.requestFocus();
		repaint();
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					dictionary = GameMonitor.gameMonitor.getDictionary();
					repaint();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		t.start();
		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				GameMonitor.gameMonitor.waitForGameStart();
				monitor.requestGameStart();
				startButnto.setEnabled(false);
				startButnto.setText(">>> Spelet har startat! <<<");
			}
		});
		t2.start();
		/*
	    Thread t3 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				GameMonitor.gameMonitor.waitForGameEnd();
				startButnto.setEnabled(true);
				startButnto.setText("Starta Spelet!");
			}
		});
	    t3.start();
		 */
	}

	@Override 
	public void paint(Graphics g){	
		int nbrTurtles = monitor.racers.size();
		if(nbrTurtles != last_num_racers){

			super.paintComponents(g);
		}
		last_num_racers = nbrTurtles;
		if(dictionary.length()>0)
			text.setText(dictionary.substring(monitor.currPos));
		if(monitor.currPercent>=100){
			text.setText("");
		}
		text.repaint();


		int i = 1;
		int k = 1;
		int bot = GUI.height-80;
		double step = (bot)/100;
		for(Turtle t: monitor.racers){		
			t.getUser();
			labels.get(i-1).setText(t.getUser());
			labels.get(i-1).repaint();
			g.setColor(t.getTurtleColor());

			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(new BasicStroke(10));

			int xpos = 40+k*((GUI.width)/8);

			g.drawLine(xpos, bot, xpos, bot-(int)(step*t.getPercent()));

			i++;
			k+=2;
		}

		for(;i<5;i++){
			labels.get(i-1).setText("");
			labels.get(i-1).repaint();
		}
		//super.requestFocus();
	}


	@Override
	public void update(Observable o, Object arg) {

		// TODO Auto-generated method stub	
		if(o instanceof Turtle){
			if(arg.equals("welcome") || arg.equals("delete") || arg.equals("color")){
			}
			else if(arg.equals("winner")){
				Turtle curr_turtle = (Turtle)o;
				JOptionPane.showMessageDialog(this, curr_turtle.getUser() +  " vann!\n\nTid det tog för " +curr_turtle.getUser()+" att bli färdig:\n" + curr_turtle.getFormatedRaceTime(), "Vinnaren är...", -1);
			}
			else{
				//Here we should find out the new percent the turtle has moved to, updates its x and y coordinated and draw them 
				// accordingly to the gui 
				Turtle curr_turtle = (Turtle)o;
				//System.out.println("Turtle did move, the overall progress is " + curr_turtle.getPercent());
			}
			//this.repaint();

		}
		this.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		monitor.newKeyEvent(e.getKeyChar());
		// System.out.println(e.getKeyChar());
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
