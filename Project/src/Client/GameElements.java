package Client;

import java.awt.Color;

public class GameElements {
	public static final int COUNT_COLORS = 4;
	public static final int COUNT_TEXTS = 4;
	public GameElements(){

	}
	public synchronized static String getDictionary(int textID){
		/**
		 * Textkälla: http://www.ungafakta.se/lattlast/
		 */ /*
		switch (textID) {
		case 0:
			return "tja";
		case 1:
			return "ha";
		case 2:
			return "ne";
		case 3:
			return "at";
		}

		return "<SERVER FEL>";
		/**/
		switch (textID) {
		case 0:
			return "När man först skulle beskriva dinosaurierna trodde man att de såg ut som stora ödlor eller krokodiler. "+
			"De fick därför namnet dinosaurier, vilket betyder fruktansvärda ödlor. "+
			"Nu vet man att de var smidiga djur. "+
			"Många gick på två ben och var väldigt snabba.";
		case 1:
			return "Från början hade de flesta pirater varit vanliga sjömän. "+
			"När deras fartyg blivit attackerat av pirater hade de kanske fått välja mellan att dö på fläcken, eller att själva bli pirater. "+
			"Då var nog valet inte så svårt. "+
			"Många blev också frivilligt pirater.";
		case 2:
			return "Många länder skickade ut slagskepp för att jaga och fånga in de farligaste piraterna. "+
			"Flera pirater dog under dessa strider. "+
			"Men de flesta pirater dog av sjukdomar ombord på skeppet. "+
			"Eller dog de om skeppet sjönk i en storm.";
		case 3:
			return "Sjömän var alltid rädda för piratanfall. "+
			"Många av piraterna var otroligt grymma. "+
			"Det kunde hända att man fick fingret avskuret, eftersom en pirat ville ha ringen som satt på det. "+
			"Många blev torterade eller dödade. "+
			"Andra kom undan utan att ett hår krökts på deras huvud. "+
			"Bland pirater hittar man alla sorters människor som hjältar, skurkar och riktiga monster.";
		case 4:
			return "Det är solens dragningskraft som håller fast planeterna i deras banor. "+
			"De fyra första planeterna har hårda ytor. De består mest av sten. "+
			"Jupiter, Saturnus, Uranus och Neptunus består av gas. De är mycket större än de andra planeterna. "+
			"De kallas ofta för gasjättar.";
		case 5:
			return "Det finns en sol och 8 planeter i vårt solsystem. "+
			"Planeterna snurrar runt solen i banor. "+
			"Planeterna ligger i den här ordningen: Merkurius, Venus, Jorden, Mars, Jupiter, Saturnus, Uranus, Neptunus. "+
			"Tidigare fanns en planet till. Den hette Pluto. Nu kallas den dvärgplanet.";
		case 6:
			return "En av de viktigaste naturkrafterna i universum är gravitationen. "+
			"Det kallas även dragningskraft eller tyngdkraft. "+
			"Det är den som gör att vi inte ramlar av jordens yta. "+
			"Allting dras mot jordens mitt; du själv, hus, bilar, träd och marken du står på.";
		case 7:
			return "För att kunna bli astronaut måste man ha nästan perfekt hälsa. "+
			"Det är vid uppskjutningen och återfärden som kroppen får störst påfrestning. Då är farten hög. "+
			"Träningen på jorden gör man i stolar som slungas runt i våldsam fart. "+
			"Det är viktigt att astronauterna bygger upp sina muskler. "+
			"De tränar hårt och lär sig vara noga med vad de äter. Det är för att vänja sig vi livet i rymden.";
		case 8:
			return "Vulkanutbrott kan ställa till med stor skada. "+
			"Hela städer har försvunnit och skogar har brunnit ner. "+
			"När människor dör under ett vulkanutbrott är det oftast inte av den heta lavan. "+
			"Lavan rinner långsamt. Det är vulkanens gaser som kan vara mycket giftiga. "+
			"Den mest kända staden som gått under är Pompeji i Italien.";
		case 9:
			return "Vulkanutbrott kan delas upp i två typer. Det beror på vilken sorts utbrott den har. "+
			"Den ena typen har ett explosivt förlopp. Den andra typen har ett lugnt förlopp. "+
			"Det är mängden kisel i magman som bestämmer hur vulkanen kommer att bete sig. "+
			"Magman tränger genom jordskorpan. Den bildar en tunnel som kallas lavarör. Ur lavaröret sprutar magman upp.";
		}
		return "<SERVER FEL>";//*/

	}
	public static Color getColorFromInt(int colorID){
		switch (colorID){
		case 3:
			return Color.GREEN;
		case 2:
			return Color.BLUE;
		case 1:
			return Color.RED;
		case 0:
			return Color.magenta;
		}
		return Color.YELLOW;
		
	}
}
