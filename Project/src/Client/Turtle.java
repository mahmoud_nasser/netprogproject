package Client;
import java.awt.Color;
import java.util.Observable;
import java.util.Random;


public class Turtle extends Observable{

	private Color color;
	private int dir;
	private String turtle_name;
	private int percentFinished;
	private int trackPos;
	private long raceTime;

	
	public Turtle(String turtle_name, Color color){
		this.color = color;
		dir = 90;
		this.turtle_name = turtle_name;
		percentFinished = 0;
		raceTime = 0;
	}
	
	public void newTurtle(){
		setChanged();
		notifyObservers("welcome");
	}
	
	public void onDeleteTurtle(){
		setChanged();
		notifyObservers("delete");
	}
	
	/*
	 * Returns the turtles name
	 */
	public String getUser(){
		return turtle_name;
	}
	/*
	 * This method should tell its observers that its time to move the turtle, 
	 * if the percent has changed the position should move
	 */
	public void setPercent(int percent){
		super.setChanged();
		percentFinished = percent;		
		notifyObservers(percent);
		
	}
	
	public int getPercent(){
		return percentFinished;
	}
	
	public void setDirection(int direction){
		dir = direction;
	}
	
	public int getdirection(){
		return dir;
	}
	
	public void setTurtleColor(Color color){		
		this.color = color;
		setChanged();
		notifyObservers("color");
	}
	
	public Color getTurtleColor(){
		return color;
	}
	
	public void setTrackPos(int x) {
		trackPos = x;
	}
	
	public int getTrackPos() {
		return trackPos;
	}
	
	public void setRaceTime(long raceTime){
		this.raceTime = raceTime;
		setChanged();
		notifyObservers("winner");
	}
	
	public long getRaceTime(){
		return raceTime;
	}
	public String getFormatedRaceTime(){
		return raceTime/(1000*60)+" min, "+(raceTime%(1000*60))/1000+ " sec,"+(raceTime%(1000*60))%1000+" ms";

	}
	
	
	
	/*
	 * Använd denna kod nedan i GUI.java så att den kan avgöra hur en turtle bör ritas upp basserat på x,y positionerna
	 * 
	 * 
	 */
//	public void correctMove(){
//		int cx = x;
//		int cy = y;
//		int[] np = newPos(dir);
//		x = np[0];
//		y = np[1];
//		gui.addLine(cx, cy, np[0], np[1], color);
//	}
//	
//	
//	public void wrongMove(){
//		int cx = x;
//		int cy = y;
//		double dir = getWrongDirection();
//		int[] np = newPos(dir);
//		x = np[0];
//		y = np[1];
//		gui.addLine(cx, cy, np[0], np[1], color);
//	}
//	
//	private int[] newPos(double dir){
//		int[] np = new int[2];
//		np[0] = x + (int)(Math.cos(Math.toRadians(dir))*step);
//		np[1] = y -((int)(Math.sin(Math.toRadians(dir))*step));
//		return np;
//	}
//
//	private double getWrongDirection(){
//		System.out.println(x + "-" + y + " ---- " + gp.nearest(x, y));
//		switch(gp.nearest(x, y)){
//		case Gameplan.LEFT:
//			return randomDirection(90,-90);
//		case Gameplan.RIGHT:
//			return randomDirection(270,90);
//		case Gameplan.LEFT_BOT:
//			return randomDirection(90,0);
//		case Gameplan.RIGHT_BOT:
//			return randomDirection(0,-90);
//		case Gameplan.BOT:
//			return randomDirection(180,0);
//		case Gameplan.NONE:
//			return randomDirection(360,0);
//		}
//		return 90.0;
//	}
//	
//	private static double randomDirection(int max, int min){
//		Random r = new Random();
//		return (min + (max - min) * r.nextDouble());
//	}
}
