package Client;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Observer;

public class GameMonitor {
	public static GameMonitor gameMonitor;
	public ArrayList<Turtle> racers;
	public ArrayList<Observer> observers;
	private String local_user="";
	public long startTime = -1;
	public long endTime = -1;
	public int currPos = 0;
	public int currPercent = 0;
	
	private char recentKeyEvent;
	private boolean isGameRunning = false;
	private boolean newKeyEventOccured = false;
	private boolean isGameStartRequested = false;
	private String dictionary;
	private boolean dictionaryEventOccured = false;
	private boolean newPercentUpdate = false;
//	Post-game info from client
	long reportedWinningTime = -1;
	String reportedWinner = "";
	
	public GameMonitor() {
		this.gameMonitor = this;
		racers = new ArrayList<Turtle>();
		observers = new ArrayList<Observer>();
		dictionary = "";
	}

	public synchronized void setLocalUser(String user) {
		local_user = user;
		notifyAll();
	}
	
	public synchronized String waitForUserNameValidation() throws InterruptedException{
		while(local_user.equals("")){
			wait();
		}
		return local_user;
	}
	public synchronized String getLocalUserName(){
		return local_user;
	}
	public synchronized void requestGameStart(){
		isGameStartRequested = true;
		notifyAll();
	}
	public synchronized void awaitGameStartRequest(){
		while(!isGameStartRequested){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void publishFinishTime(String user, long finishTime){
		for(Turtle t : racers){
			if(t.getUser().equals(user)){
				t.setRaceTime(finishTime);
			}
			
		}
	}

	public synchronized void addRacer(String user, Color userColor) {
		racers.add(new Turtle(user, userColor));
		racers.get(racers.size()-1).setTrackPos(racers.size());
		addWatcher();
		racers.get(racers.size()-1).newTurtle();
	}
	
	public synchronized void removeRacer(String user){
		for(Turtle t: racers){
			if(t.getUser().equals(user)){
				t.onDeleteTurtle();
				t.deleteObservers();				
				racers.remove(t);
				break;
			}
		}
	}

	public synchronized void moveTurtle(String user, int percentCompleted) {
		for (int i = 0; i < racers.size(); i++) {

			if (racers.get(i).getUser().equals(user)) {
				int old_percent = racers.get(i).getPercent();
				racers.get(i).setPercent(percentCompleted);
				if(percentCompleted > old_percent){
					newPercentUpdate = true;
					notifyAll();
				}
			}
			
			
		}
	}
	
	public synchronized void disconnectRacer(String user){
		for(Turtle t : racers){
			if(t.getUser().equals(user)){
				t.setTurtleColor(Color.DARK_GRAY);
			}
		}
	}

	public synchronized char waitForGUIInput() throws InterruptedException {
		while (!newKeyEventOccured) {
			wait();
		}
		newKeyEventOccured = false;
		return recentKeyEvent;
	}

	public synchronized void newKeyEvent(char key) {
		recentKeyEvent = key;
		newKeyEventOccured = true;
		notifyAll();
	}

	public synchronized void setDictionary(String dictionary) {
		this.dictionary = dictionary.trim();
		dictionaryEventOccured = true;
		notifyAll();
	}

	public synchronized String getDictionary() throws InterruptedException {
		while (!dictionaryEventOccured) {
			wait();
		}
//		dictionaryEventOccured = false;
		return dictionary;
	}

	private void addWatcher() {
		for (int i = 0; i < racers.size(); i++) {
			racers.get(i).deleteObservers();
			for (int k = 0; k < observers.size(); k++) {
				racers.get(i).addObserver(observers.get(k));
			}
		}
	}

	public synchronized void addObserver(Observer watch) {
		observers.add(watch);
		addWatcher();
	}
	public synchronized void startRace() {
		startTime = System.currentTimeMillis();
		isGameRunning = true;
		notifyAll();
	}
	public synchronized void finishedRace() {
		endTime = System.currentTimeMillis();
		isGameRunning = false;
		notifyAll();
	}
	public synchronized void waitForGameStart(){
		try {
			while(!isGameRunning){
				wait();
			}
			startRace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public synchronized void waitForGameEnd(){
		try {
			while(isGameRunning){
				wait();
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public synchronized boolean gameIsRunning() {
		return isGameRunning;
	}
	public synchronized long waitForReadyRaceTime() {
		while(endTime == 0){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return (endTime - startTime);
	}

	public synchronized void firstPlayerInGoal() {
		// TODO Implement. Denna anropas n�r f�rsta medspelaren n�tt m�l :P
		
	}
	public synchronized boolean IhaveFinishedTheRace() {
		return currPercent >= 100;
		
	}
	public synchronized int waitForPercentUpdate(){
		while(!newPercentUpdate){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Fel vid procent-updatering");
				System.exit(-1);
			}
		}
		newPercentUpdate = false;
		return currPercent;
	}
	

	public synchronized int getCurrentPercent(){
		return currPercent;
	}

	public synchronized void waitForServerToTellUsWhoTheWinnerIs() {
		while(reportedWinningTime == -1){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
