package Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**

 */
public class ClientConnection extends Thread{
	private OutputStream output;
	private CCReceiver rec;
	private Socket clientSocket = null;
	private String userName = "";
	private int color = -1;
	private int advancement; //advancement from 1-100 
	private long raceTime = -1;

	private boolean announcedToAll = false;

	/*
	 * COMMANDS FROM CLIENT TO SERVER
	 */
	public static final int C_SET_USERNAME = 1;			// 1 argument(s) (string username)
	public static final int C_START_GAME = 2;			// 0 argument(s) Request	
	public static final int C_SET_ADVANCEMENT = 3;		// 1 Argument(s) (int myAdvancement 1-100)
	public static final int C_GET_DICTIONARY = 5;		// 0 argument(s) Request
	public static final int C_REPORT_RACETIME = 6;		// 1 argument(s) (long endTime-starTime in millis) 
	/*
	 * COMMANDS FROM SERVER TO CLIENT
	 */
	public static final int S_NEW_USER = 201;			// 2 argument(s) (string user, int color)
	public static final int S_USERNAME_NONVALID = 202;	// 0 argument(s) Confirmation
	public static final int S_GAME_STARTED = 203;		// 1 argument(s) ((bool)int gameIsStarted)	
	public static final int S_SET_ADVANCEMENT = 204;	// 2 Argument(s) (string user, int advancement)
	public static final int S_USER_DISCONNECTED = 205;	// 1 argument(s) (string user)
	public static final int S_WINNER = 206;				// 2 argument(s) (string user, int wintimeInMillis)
	public static final int S_DICTIONARY = 208;			// 1 argument(s) (int textID)
	public static final int S_ERROR	 = 300;				// 0/1 Argument(s) (string errorMSG)
	public static final int S_REJECTED = 301;			// 0 Argument(s) Info
	/*
	 * GLOBAL COMMANDS
	 */
	public static final int G_TERMINATE = 0;			// 0 argument(s)


	public ClientConnection(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}
	/**
	 * Sends a message to the corresponding client the applying ClientConnecton is responsible for
	 * @param msg The message to be sent. Use the command integers with the S_ prefix.
	 */
	protected synchronized void sendMessage(String msg) {
		try {
			OutputStreamWriter ow = new OutputStreamWriter(output);
			BufferedWriter bw = new BufferedWriter(ow);
			System.out.println("Sending: "+msg);
			bw.write(msg+"\r\n");
			bw.flush();
			/*output.write((msg).getBytes());
			output.flush();*/
		} catch (IOException e) {
			//Closed connection? Disconnected!
			terminate();
			e.printStackTrace();
		}
	}
	/**
	 * Changes the flag for announcemnt, which describes whether the current client has been announced to users or not
	 * @param isAnnouncedToOtherUsers Set to true when server has broadcasted S_NEW_USER to all clients.
	 */
	protected synchronized void setAnnouncement(boolean isAnnouncedToOtherUsers) {
		this.announcedToAll = isAnnouncedToOtherUsers;
	}
	/**
	 * Check if corresponding client has been announced to other active clients
	 * @return Returns true if this is the case
	 */
	protected synchronized boolean getAnnouncement() {
		return this.announcedToAll;
	}
	/**
	 * Set (checked and unique) username for the corresponding client
	 * Notifies all threads waiting in ClientConnection
	 * @param name The name to set
	 */
	public synchronized void setUserName(String name) {
		this.userName = name;
		notifyAll();
	}
	/**
	 * Get corresponding clients username
	 * @return Returns clients username, returns an empty string if no unique name has been accepted yet.
	 */
	protected synchronized String getUserName() {
		return this.userName;
	}
	/**
	 * Sets a color for the corresponding client/player
	 * @param color The color ID to set
	 */
	protected synchronized void setUserColor(int color) {
		this.color = color;
	}
	/**
	 * Gives the corresponding client/player's set color ID
	 * @return Returns a color ID, if none is set -1 is returned
	 */
	protected synchronized int getUserColor() {
		return this.color;
	}
	/**
	 * Terminates the corresponding client in a safe way.
	 */
	protected synchronized void terminate(){
		try {
			output.close();
			rec.close();
			clientSocket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Server.server.terminateMe(this);
	}
	/**
	 * Blocks until a proposition of a valid username from the client is set.
	 * @return Returns the current username
	 */
	public synchronized String waitForUserName(){
		while(userName.equals("")){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return userName;
	}
	/**
	 * Gives the time it took to complete a race in milliseconds
	 * @return Returns the time in ms. Returns -1 if corresponding player hasn't completed the race
	 */
	public synchronized long getRaceTime(){
		return raceTime;
	}
	/**
	 * Decodes and executes a given command and their potential arguments received from the client-side.
	 * @param command The full command with its potential arguments
	 */
	public  void execute(String command){
		String[] strings = command.split("##");
		int com = -1;
		try{
			com = Integer.parseInt(strings[0].trim());
		}catch(NumberFormatException e){
			System.out.println("Command was not a valid integer");
		}
		catch(Exception e){
			System.out.println("Other error...");
		}
		switch(com){
		case -1 :
			System.out.println("Execute: Command Parse Failure");
			break;

		case ClientConnection.G_TERMINATE :
			terminate();
			break;
		case ClientConnection.C_SET_USERNAME :
			if(strings.length < 2){
				System.out.println("Execute: Arguments Not Present");
				sendMessage(ClientConnection.S_ERROR+"##"+"Arguments Missing");
				setUserName("");
				sendMessage(ClientConnection.S_USERNAME_NONVALID+"");
				break;
			}
			if(Server.server.isUserNameAvailableAndValid(strings[1])){
				setUserName(strings[1]);
				Server.server.announceNewConnection();
			}
			else{
				setUserName("");
				sendMessage(ClientConnection.S_USERNAME_NONVALID+"");
			}
			break;
		case ClientConnection.C_SET_ADVANCEMENT:
			if(strings.length < 2){
				System.out.println("Execute: Arguments Not Present");
				sendMessage(ClientConnection.S_ERROR+"##"+"Arguments Missing");
				break;
			}
			try{
				int procentMul10 = Integer.parseInt(strings[1]);
				if(procentMul10 < 0 || procentMul10 > 100){
					throw new NumberFormatException();
				}
				advancement = procentMul10;
				Server.server.sendMessageToAll(ClientConnection.S_SET_ADVANCEMENT+"##"+userName+"##"+advancement);
			}catch(NumberFormatException e){
				System.out.println("Execute: Argument Not a Valid long (integer), or not in range (0-100");
				sendMessage(ClientConnection.S_ERROR+"##"+"Invalid Argument");
			}
			catch(Exception e){
				System.out.println("Execute: Other error");
				sendMessage(ClientConnection.S_ERROR+"##"+"Unknown error occured when parsing your request on the server");
			}
			break;
		case ClientConnection.C_GET_DICTIONARY:
			sendMessage(ClientConnection.S_DICTIONARY+"##"+Server.server.getTextID());
			break;
		case ClientConnection.C_REPORT_RACETIME:
			if(strings.length < 2){
				System.out.println("Execute: Arguments Not Present");
				sendMessage(ClientConnection.S_ERROR+"##"+"Arguments Missing");
				break;
			}
			try{
				long raceTime = Long.parseLong(strings[1]);
				if(raceTime < 0){
					throw new NumberFormatException();
				}
				this.raceTime = raceTime;
				//Här e kodandet fett skumt, följer inga bra regler alls. Problemet? Vi måste skicka ut detta värdet till alla
				//Server.server.notifyAll(); // <<< är det den som krashar måntro? hmm låt mig felsöka? mm
				Server.server.notifyServerOnNewRaceTime();
			}catch(NumberFormatException e){
				System.out.println("Execute: Argument Not a Valid long (integer), or not in range (0-100");
				sendMessage(ClientConnection.S_ERROR+"##"+"Invalid Argument");
			}

		case ClientConnection.C_START_GAME :
			Server.server.startGame();
			break;
		}
	}

	@Override
	public void run() {

		try {
			output = clientSocket.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			rec = new CCReceiver(br, this);
			rec.start();
			//await userName from client to go on
			if(!clientSocket.isClosed()){
				//Server.server.announceNewConnection();
				Server.server.discoverAlreadyConnectedUsers(this);
			}
			if(!clientSocket.isClosed()){
				waitForUserName();
			}
			while(!clientSocket.isClosed() && !Server.server.isGameStarted()){
				Server.server.updatePlayersUntilGameStart();
			}
			if(!clientSocket.isClosed() && Server.server.isGameStarted()){
				Server.server.waitForGameFinish();  //Game end for the server all has reported a time
				Server.server.determineTheWinner();//Determine the winner
			}
			//Termination goes here...
			if(!clientSocket.isClosed()){
				Server.server.requestMyTermination(this);
			}
		} catch (IOException e) {
			System.out.println("No connection");
			try {
				clientSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
				System.exit(-1);
			}

		}


	}

}
