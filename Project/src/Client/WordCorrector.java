package Client;

import java.util.Observable;
import java.util.Observer;

public class WordCorrector extends Thread {
	private String dictionary;
	private int currentPos, currentWord;
	private String[] tmp;
	public WordCorrector() {
		this.dictionary = "";
		currentPos = 0;
		currentWord = 0;
	}

	private boolean isInputCorrect(char input) {
		if (dictionary.charAt(currentPos) == input) {
			/* ^^ Skiftläge-Känslig ^^    --- --- --- vv Skiftläge-Okänslig vv
		if (dictionary.toUpperCase().charAt(currentPos) == input || dictionary.toLowerCase().charAt(currentPos) == input) {
			 */
			if (currentPos + 1 == dictionary.length()) {
				currentWord++;
				GameMonitor.gameMonitor.finishedRace();
			} else if (dictionary.charAt(currentPos + 1) == ' ') {
				currentWord++;
			}
			currentPos++;
			return true;
		}
		if((input>'a' && input<'z') || input == 'å'|| input == 'ä'|| input == 'ö' || (input>'A' && input<'Z')|| input == 'Å'|| input == 'Ä'|| input == 'Ö' ){
			java.awt.Toolkit.getDefaultToolkit().beep();
		}
		return false;
	}

	public int getPercentCompleted() {
		return (int) Math.round(100.0 * (currentWord / (1.0 * tmp.length)));
	}

	@Override
	public void run() {
		try {
			dictionary = GameMonitor.gameMonitor.getDictionary();
			tmp = dictionary.split(" ");
			GameMonitor.gameMonitor.waitForGameStart();
			while (currentPos < dictionary.length()) {

				char guiInput = GameMonitor.gameMonitor.waitForGUIInput();
				if (isInputCorrect(guiInput)) {
					// Det blev rätt flytta fram
					GameMonitor.gameMonitor.currPos = currentPos;
					GameMonitor.gameMonitor.moveTurtle(GameMonitor.gameMonitor.getLocalUserName(), getPercentCompleted());
					GameMonitor.gameMonitor.currPercent = getPercentCompleted();
				}
			}
		} catch (Exception e) {

		}
	}

}
