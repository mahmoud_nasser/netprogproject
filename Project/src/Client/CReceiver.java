package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CReceiver extends Thread{
	BufferedReader br;
	Client myCl;
	public CReceiver(BufferedReader br, Client cl){
		this.br = br;
		this.myCl = cl;
	}

	@Override
	public void run() {
		try {
			
			String msg = br.readLine();
			while(msg != null){
				System.out.println("Recieved: "+msg);
				myCl.execute(msg);
				//notifyAll();
				msg = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("Lost connection to server, terminating...");
			try {
				br.close();
			} catch (IOException e1) {
				System.out.println("Something really bad happened!");
				e1.printStackTrace();
			}
		}
	}
	public void close(){
		try {
			br.close();
		} catch (IOException e1) {
			System.out.println("Something really bad happened!");
			e1.printStackTrace();
		}
	}
//	public synchronized void waitForIt(){
//		try {
//			wait();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}
